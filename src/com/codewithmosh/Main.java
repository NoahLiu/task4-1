package com.company;
import java.util.ArrayList;
import java.util.Scanner;  // Import the Scanner class

public class Main {

    public static void main(String[] args) {
	// write your code here
        ArrayList<String> myAirplane = new ArrayList<String>();

        System.out.println("method add:");
        myAirplane.add("F-22 Raptor");
        myAirplane.add("J-20 Firefang");
        myAirplane.add("F-35 Lightning II");
        myAirplane.add("SU-57  Felon");

        System.out.println(myAirplane);


        //method get and size
        System.out.println("method get and size:");
        for(int i=0; i< myAirplane.size(); i++) {
            System.out.println(myAirplane.get(i));
        };

        //method remove
        System.out.println("method remove:");
        Scanner myObj = new Scanner(System.in);  // Create a Scanner object
        System.out.println("Enter which number you need remove, it should less than " + myAirplane.size());
        int indexInput = myObj.nextInt();
        myAirplane.remove(indexInput-1);
        System.out.println(myAirplane);

        //method clearALL
        System.out.println("method clearAll:");
        myAirplane.clear();
        System.out.println(myAirplane);

    }
}
